import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { QuillModule } from 'ngx-quill';
import { FormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { MonacoEditorModule } from 'ngx-monaco-editor';
import { HtmlEditorComponent } from './html-ediotor/html-editor.component';
import { UploaderComponent } from './uploader/uploader.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ModalEditorDirective } from './directives/modal-editor.directive';
import { MatDialogModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditableContentComponent } from './editable-content/editable-content.component';
import { EditDialogComponent } from './edit-dialog/edit-dialog.component';
import { SnapshotGroupComponent } from './snapshot-group/snapshot-group.component';
import { SnapshotDirective } from './directives/snapshot.directive';

@NgModule({
    declarations: [
        AppComponent,
        HtmlEditorComponent,
        UploaderComponent,
        ModalEditorDirective,
        EditableContentComponent,
        EditDialogComponent,
        SnapshotGroupComponent,
        SnapshotDirective,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        CKEditorModule,
        NgxDropzoneModule,
        MatDialogModule,
        QuillModule.forRoot(),
        MonacoEditorModule.forRoot(),
        BrowserAnimationsModule
    ],
    providers: [],
    bootstrap: [
        AppComponent
    ],
    entryComponents: [
        EditDialogComponent
    ]
})
export class AppModule {
}

