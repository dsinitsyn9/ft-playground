import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { EditDialogComponent } from '../edit-dialog/edit-dialog.component';
import { MatDialog } from '@angular/material';
import { DialogEditorTypes } from '../types/dialog-editor.types';

@Directive({
    selector: '[appModalEditor]'
})
export class ModalEditorDirective {
    @Input() type: DialogEditorTypes;
    @Input() content: any;
    @Input() title: string;
    @Output() change = new EventEmitter<any>();

    constructor(public dialog: MatDialog) {
    }

    @HostListener('click', ['$event']) onClick($event) {
        this.openDialog();
    }


    openDialog() {
        const dialogRef = this.dialog.open(EditDialogComponent, {
            width: '700px',
            data: {
                type: this.type,
                content: this.content,
                title: this.title
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.change.next(result);
            }
        });
    }
}
