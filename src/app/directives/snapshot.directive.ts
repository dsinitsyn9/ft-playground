import { Directive, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { SnapshotGroupService } from '../services/snapshot-group.service';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[snapshot]'
})
export class SnapshotDirective implements OnChanges, OnInit {
    // tslint:disable-next-line:variable-name
    _snapshot = null;
    initialValue: any;
    isValueChanged = false;
    @Output() snapshotChange = new EventEmitter();
    get snapshot() {
        console.log('get');
        return this._snapshot;
    }

    // tslint:disable-next-line:adjacent-overload-signatures
    @Input() set snapshot(val) {
        this._snapshot = val;
        this.snapshotChange.emit(this._snapshot);
    }

    constructor(private $snapshot: SnapshotGroupService) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.snapshot.firstChange) {
            this.initialValue = changes.snapshot.currentValue;
        }

        if (changes.snapshot.currentValue !== this.initialValue) {
            if (!this.isValueChanged) {
                this.$snapshot.onSnapshotChanged.next(true);
                this.isValueChanged = true;
            }
        } else {
            if (this.isValueChanged) {
                this.$snapshot.onSnapshotChanged.next(false);
                this.isValueChanged = false;
            }
        }
    }

    ngOnInit() {
        this.$snapshot.onRollback.subscribe(() => {
            this.snapshot = this.initialValue;
        });
    }

}
