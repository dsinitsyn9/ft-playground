import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-editable-content',
    templateUrl: './editable-content.component.html',
    styleUrls: ['./editable-content.component.scss']
})
export class EditableContentComponent implements OnInit {
    html = `<p>Test!</p>`;
    css = `body{font-size: 14px}`;

    constructor() {
    }

    ngOnInit() {
    }
}
