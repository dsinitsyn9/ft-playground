import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import Quill from 'quill';


@Component({
    selector: 'app-html-editor',
    templateUrl: './html-editor.component.html',
    styleUrls: ['./html-editor.component.scss']
})
export class HtmlEditorComponent {
    @Input() text = '<p>Hello <strong>{guestName}</strong>!</p><p>Welcome to {hotelName}!</p>';
    @Output() change = new EventEmitter<string>();
    quillEditor: Quill;
    monacoEditor;
    sourceCode = false;
    quillConfig = {
        toolbar: {
            toolbar: '#toolbar',
            container: [
                ['bold']
            ]
        }
    };
    monacoOptions = {
        formatOnPaste: true,
        formatOnType: true,
        autoIndent: true,
        theme: 'vs-dark',
        language: 'html'
    };
    tokens = [
        {
            name: 'Guest name',
            value: '{guestName}'
        },
        {
            name: 'Hotel name',
            value: '{hotelName}'
        }
    ];
    selectedToken = null;

    constructor() {
    }

    quillEditorCreated(event) {
        this.quillEditor = event;
    }

    monacoEditorCreated(event) {
        this.monacoEditor = event;

        setTimeout(() => {
            this.monacoEditor.getAction('editor.action.formatDocument').run();
        }, 50);
    }

    changeFormat() {
        this.sourceCode = !this.sourceCode;
    }

    onTokenSelected(token: string) {

        if (this.sourceCode) {
            this.insertTokenInMonaco(token);
        } else {
            this.insertTokenInQuill(token);
        }

        setTimeout(() => {
            this.selectedToken = null;
        });
    }

    insertTokenInQuill(token: string) {
        if (token) {
            const selection = this.quillEditor.getSelection();
            const cursorPosition = selection ? selection.index : 0;
            this.quillEditor.insertText(cursorPosition, token);
            this.quillEditor.setSelection(cursorPosition + token.length);
            // this.updateQuillText();
        }
    }

    onChange() {
        // console.log(this.text);
        // this.change.next(this.text);
    }

    insertTokenInMonaco(token: string) {
        if (token) {
            const selection = this.monacoEditor.getSelection();
            const range = new monaco.Range(selection.startLineNumber, selection.startColumn, selection.endLineNumber, selection.endColumn);
            const insertToken = {range, text: token, forceMoveMarkers: true};
            this.monacoEditor.executeEdits('tokens', [insertToken]);
        }
    }
}
