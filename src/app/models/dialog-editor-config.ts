import { DialogEditorTypes } from '../types/dialog-editor.types';

export interface DialogEditorConfig {
    type: DialogEditorTypes;
    content: any;
    title: string;
}
