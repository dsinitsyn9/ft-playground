import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class SnapshotGroupService {
    onRollback = new Subject();
    onSnapshotChanged = new Subject();

    constructor() {

    }
}
