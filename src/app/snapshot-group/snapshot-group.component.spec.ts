import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnapshotGroupComponent } from './snapshot-group.component';

describe('SnapshotGroupComponent', () => {
  let component: SnapshotGroupComponent;
  let fixture: ComponentFixture<SnapshotGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnapshotGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnapshotGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
