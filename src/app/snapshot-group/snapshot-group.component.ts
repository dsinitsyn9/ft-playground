import { ChangeDetectorRef, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { SnapshotGroupService } from '../services/snapshot-group.service';

@Component({
    selector: 'app-snapshot-group',
    templateUrl: './snapshot-group.component.html',
    styleUrls: ['./snapshot-group.component.scss'],
    providers: [SnapshotGroupService]
})
export class SnapshotGroupComponent implements OnInit {
    changesAmount = 0;
    @Output() commit = new EventEmitter();

    constructor(private $snapshotService: SnapshotGroupService, private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {
        this.$snapshotService.onSnapshotChanged.subscribe(changed => {
            if (changed) {
                this.changesAmount++;
            } else {
                this.changesAmount--;
            }
            this.cdr.detectChanges();
        });
    }

    rollBack() {
        this.$snapshotService.onRollback.next();
    }

}
