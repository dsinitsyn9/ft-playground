import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';


interface ImageFile {
    source: File;
    width: number;
    height: number;
    base64: string | ArrayBuffer;
}

@Component({
    selector: 'app-uploader',
    templateUrl: './uploader.component.html',
    styleUrls: ['./uploader.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class UploaderComponent implements OnInit {
    @Input() maxHeight = 1000;
    @Input() maxWidth = 1000;
    images: ImageFile[] = [];

    constructor() {
    }

    ngOnInit() {
    }

    onSelect(event) {
        event.addedFiles.forEach((file: File) => {
            this.createImageFile(file).then((imageFile: ImageFile) => {
                if (this.validateImageResolution(imageFile)) {
                    this.images.push(imageFile);
                } else {
                    alert(`${file.name}: Wrong image resolution`);
                }
            }).catch(err => {
                alert(`${file.name}: ${err}`);
            });
        });
    }

    onUpload() {
        this.images.forEach((image: ImageFile) => {
            console.log(image);
        });
    }

    validateImageResolution(image: ImageFile): boolean {
        return image.height <= this.maxHeight && image.width <= this.maxWidth;
    }

    onRemove(event) {
        this.images.splice(this.images.indexOf(event), 1);
    }

    createImageFormData(image: ImageFile) {
        const formData = new FormData();
        formData.append('image', image.source);
    }

    createImageFile(file: File): Promise<ImageFile> {
        return this.getImageBase64(file).then((base64: string) => {
            return this.getImageResolution(base64).then(({width, height}) => {
                return {
                    source: file,
                    height,
                    width,
                    base64
                };
            });
        });
    }

    getImageBase64(file: File): Promise<string> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                const base64 = reader.result as string;
                resolve(base64);
            };
            reader.onerror = () => {
                reject('Error reading an image');
            };
        });
    }

    getImageResolution(base64: string): Promise<{ height: number, width: number }> {
        return new Promise((resolve, reject) => {
            const image = new Image();
            image.src = base64;
            image.onload = () => {
                resolve({
                    height: image.height,
                    width: image.width
                });
            };
            image.onerror = () => {
                reject('File is not an image');
            };
        });
    }
}
